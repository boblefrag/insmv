CREATE TABLE ins_user
(
    id SERIAL PRIMARY KEY,
    name VARCHAR(100),
    group_id integer not null,    
    FOREIGN KEY (group_id) REFERENCES ins_group(id),
    PF integer

)
