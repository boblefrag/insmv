import os
import unicodedata
import json
from psycopg2 import connect, extras
from werkzeug.utils import redirect
from werkzeug.wsgi import SharedDataMiddleware
from werkzeug.exceptions import HTTPException
from werkzeug.routing import Map, Rule
from werkzeug.wrappers import Request, Response
from jinja2 import Environment, FileSystemLoader

class INS(object):
    def __init__(self, config):
        if not config["postgresql"]["user"]:
            config["postgresql"].pop("user")
            config["postgresql"].pop("password")
        self.postgresql = connect(**config["postgresql"])
        extras.register_hstore(self.postgresql)
        self.results = {}
        template_path = os.path.join(os.path.dirname(__file__), 'templates')
        self.jinja_env = Environment(loader=FileSystemLoader(template_path), autoescape=True)

        self.results = self.request_pg('sql/list_groups.sql')
        self.url_map = Map([
            Rule('/', endpoint='index'),
            Rule('/create', endpoint='create_group'),
            Rule('/<group>', endpoint='group_detail'),
            Rule('/<group>/create', endpoint='create_user'),
            Rule('/<group>/delete', endpoint='delete_group'),
            Rule('/<group>/<user>/delete', endpoint='delete_user'),
            Rule('/api/', endpoint='api_index'),
            Rule('/api/<group>', endpoint='api_group_detail'),
            Rule('/api/user/<user>', endpoint='api_user_detail'),
            Rule('/api/user/<user>/update', endpoint='api_user_update'),
        ])

    def write_pg(self, fd, params=None):
        with open(fd) as fd:
            try:
                with self.postgresql.cursor(
                        cursor_factory=extras.RealDictCursor) as cur:
                    cur.execute(fd.read(), params)
                self.postgresql.commit()
            except Exception as e:
                self.postgresql.rollback()
                raise

    def request_pg(self, fd, params=None):
        with open(fd) as fd:
            with self.postgresql.cursor(
                    cursor_factory=extras.RealDictCursor) as cur:
                cur.execute(fd.read(), params)
                return cur.fetchall()

    def dispatch_request(self, request):
        adapter = self.url_map.bind_to_environ(request.environ)
        try:
            endpoint, values = adapter.match()
            return getattr(self, 'on_' + endpoint)(request, **values)
        except HTTPException as e:
            return e

    def wsgi_app(self, environ, start_response):
        request = Request(environ)
        response = self.dispatch_request(request)
        return response(environ, start_response)

    def __call__(self, environ, start_response):
        return self.wsgi_app(environ, start_response)

    def render_template(self, template_name, **context):
        t = self.jinja_env.get_template(template_name)
        return Response(t.render(context), mimetype='text/html')


    def on_index(self, request):
        results = self.request_pg('sql/list_groups.sql')
        return self.render_template('index.html', results=results)

    def on_delete_group(self, request, group):
        results = self.write_pg('sql/delete_group.sql', (group,group))
        return redirect("/")

    def on_delete_user(self, request, group, user):
        results = self.write_pg('sql/delete_user.sql', (user,))
        return redirect("/{}".format(group))

    def on_group_detail(self, request, group):
        results = self.request_pg('sql/list_users.sql', (group,))
        return self.render_template('detail.html', results=results, group=group)

    def on_create_user(self, request, group):
        if request.method=="GET":
            results = self.request_pg('sql/list_users.sql', (group,))
            return self.render_template('create_user.html', result=results, group=group, form=None)
        elif request.method=="POST":
            form={}
            if request.form.get("name") and request.form.get("PF"):
                try:
                    form["PF"] = int(request.form["PF"])
                    form["BL"] = int(request.form["BL"])
                    form["name"] = unicodedata.normalize('NFD',request.form["name"]).encode('ascii', 'ignore')
                    self.write_pg('sql/create_user.sql', (form["name"],form["PF"],form["BL"], group ))
                    return redirect("/{}".format(group))
                except:
                    request.form.error = "PF and BL must be an integer"
            else:
                request.form.error="Missing PF, BL or Name"
            return self.render_template('create_user.html', group=group, form=request.form)

    def on_create_group(self, request):
        if request.method=="GET":
            results = self.request_pg('sql/list_groups.sql')
            return self.render_template('create_group.html', result=results, form=None)
        elif request.method=="POST":
            form={}
            if unicode(request.form.get("name")):
                form["name"] = unicodedata.normalize('NFD',request.form["name"]).encode('ascii', 'ignore')
                self.write_pg('sql/create_group.sql', (form["name"],))
                return redirect("/")
            else:
                request.form.error="Nom obligatoire"
            return self.render_template('create_group.html', form=request.form)

    def on_api_index(self, request):
        results = self.request_pg('sql/list_groups.sql')
        return Response(json.dumps(results), mimetype="application/json")

    def on_api_group_detail(self, request, group):
        results = self.request_pg('sql/list_users.sql', (group,))
        return Response(json.dumps(results), mimetype="application/json")

    def on_api_user_detail(self, request, user):
        results = self.request_pg('sql/user_detail.sql', (user,))
        return Response(json.dumps(results), mimetype="application/json")

    def on_api_user_update(self, request, user):
        if request.method=="GET":
            results = self.request_pg('sql/user_detail.sql', (user,))
            return Response(json.dumps(results), mimetype="application/json")
        elif request.method=="POST":
            content={}
            if request.form.get("bl"):
                content["bl"] = int(request.form["bl"])
                self.write_pg('sql/update_user.sql', (content["bl"],user))
                return redirect("/api/user/{}".format(user))
            else:
                request.form.error="Nom obligatoire"
            return self.render_template('create_group.html', form=request.form)


def create_app(pg_host=None, pg_port=5432, pg_database='INS',
               pg_username=None,pg_password=None,
               with_static=True):
    app = INS({
        'postgresql': {
            'host': pg_host,
            'port': pg_port,
            'database': pg_database,
            'user': pg_username,
            'password': pg_password
            }
    })
    if with_static:
        app.wsgi_app = SharedDataMiddleware(app.wsgi_app, {
            '/static':  os.path.join(os.path.dirname(__file__), 'static')
        })
    return app

if __name__ == '__main__':
    from werkzeug.serving import run_simple
    app = create_app()
    run_simple('127.0.0.1', 5000, app, use_debugger=True, use_reloader=True)

# for gunicorn:
# for example
# gunicorn -w 4 -b 0.0.0.0:5000 app:app --log-level debug
app = create_app()
